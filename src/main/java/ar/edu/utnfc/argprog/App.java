package ar.edu.utnfc.argprog;


/* Primera versión
    Calculadora implementada con un esquema orientado a objetos planteando una suerte de patrón strategy donde lo que
    cambia entre una clase y otra es la estrategia de cálculo.
 */
import ar.edu.utnfc.argprog.version.objetos.Calculadora;

/* Segunda versión
    Calculadora implementada con un esquema funcional utilizando una interfaz funcional personalizada (escrita por
    nosotos mismos).
 */
//import ar.edu.utnfc.argprog.version.funcional.custom.Calculadora;

/* Tercera versión
    Calculadora implementada con lambdas basadas en una interfaz funcional de la librería de java.
 */
//import ar.edu.utnfc.argprog.version.funcional.java.Calculadora;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Calculadora.start();

    }
}
