package ar.edu.utnfc.argprog.version.objetos;

public class Suma implements Operacion {
    @Override
    public float calcular(float a, float b) {
        return a + b;
    }
}