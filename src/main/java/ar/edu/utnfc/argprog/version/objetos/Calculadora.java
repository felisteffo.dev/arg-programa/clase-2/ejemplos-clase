package ar.edu.utnfc.argprog.version.objetos;

import java.util.Scanner;

public class Calculadora {

    public static void start() {

        Operacion[] operaciones;

        operaciones = new Operacion[]{
                new Suma(),
                new Resta(),
                new Multiplicacion(),
                new Division()
        };

        Scanner scanner = new Scanner(System.in);
        float a, b;
        int opcion;
        do {
            System.out.println("Calculadora básica");
            System.out.println("1. Suma");
            System.out.println("2. Resta");
            System.out.println("3. Multiplicación");
            System.out.println("4. División");
            System.out.println("0. Salir");
            System.out.print("Seleccione una operación: ");
            opcion = scanner.nextInt();

            if (opcion >= 1 && opcion <= 4) {
                System.out.print("Ingrese el primer operando: ");
                a = scanner.nextFloat();
                System.out.print("Ingrese el segundo operando: ");
                b = scanner.nextFloat();
                float resultado = operaciones[opcion - 1].calcular(a, b);
                System.out.println("El resultado es: " + resultado);
            }
            else
                if (opcion != 0) {
                    System.out.println("Opción inválida. Intente nuevamente.");
                }
            System.out.println("\n");

        } while (opcion != 0);

    }

}