package ar.edu.utnfc.argprog.version.funcional.custom;

@FunctionalInterface
public interface Operacion {
    float calcular(float a, float b);
}
