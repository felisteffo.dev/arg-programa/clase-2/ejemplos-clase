package ar.edu.utnfc.argprog.version.objetos;

public interface Operacion {
    float calcular(float a, float b);
}
