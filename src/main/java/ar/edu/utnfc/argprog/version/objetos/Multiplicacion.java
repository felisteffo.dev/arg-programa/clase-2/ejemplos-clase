package ar.edu.utnfc.argprog.version.objetos;

public class Multiplicacion implements Operacion {
    @Override
    public float calcular(float a, float b) {
        return a * b;
    }
}